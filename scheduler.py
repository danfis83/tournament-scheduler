#!/usr/bin/python

import collections
import copy
import functools
import itertools


def _is_string(el):
    # python2 vs python3
    try:
        # python2
        return isinstance(el, basestring)
    except NameError:
        return isinstance(el, str)


def _flatten(l):
    for el in l:
        if (isinstance(el, collections.Iterable) and not _is_string(el)):
            for sub in _flatten(el):
                yield sub
        else:
            yield el


def scheduler_1vs1(object_list):

    games = []
    obj_list = [idx for idx, obj in enumerate(object_list)]
    # count = sum(range(len(obj_list)))
    games_per_round = int(len(obj_list) / 2)

    if len(obj_list) % 2 == 0:
        # even
        #
        # 1 2  ->  1 3  ->  1 4
        # 3 4      4 2      2 3
        #
        # 1 2  ->  1 3  ->  1 5  ->  1 6  ->  1 4
        # 3 4      5 2      6 3      4 5      2 6
        # 5 6      6 4      4 2      2 3      3 5
        #
        game_rounds = len(obj_list) - 1
        _teams = copy.copy(obj_list)
        for r in range(game_rounds):
            l = copy.copy(_teams)
            for idx, g in enumerate(range(games_per_round)):
                if r % 2 == 1 and idx == 0:
                    game = (l.pop(1), l.pop(0))
                else:
                    game = (l.pop(0), l.pop(0))
                games.append(game)

            # spin the teams (clockwise)
            l = copy.copy(_teams)
            for idx, n in enumerate(l):
                if idx == 0:
                    new_idx = idx
                elif idx == 1:
                    new_idx = 3
                elif idx == 2:
                    new_idx = 1

                elif idx % 2 == 0:
                    new_idx = idx - 2
                elif idx % 2 == 1:
                    new_idx = idx == len(l) - 1 and idx - 1 or idx + 2
                _teams[new_idx] = n

    else:
        # odd
        #
        # 1 2  ->  3 1  ->  5 3  ->  4 5  ->  2 4
        # 3 4      5 2      4 1      2 3      1 5
        # 5        4        2        1        3
        #
        _games = []
        game_rounds = len(obj_list)
        _teams = copy.copy(obj_list)
        for r in range(game_rounds):
            l = copy.copy(_teams)
            for idx, g in enumerate(range(games_per_round)):
                game = (l.pop(0), l.pop(0))
                _games.append(game)

            # spin the teams (clockwise)
            l = copy.copy(_teams)
            for idx, n in enumerate(l):
                if idx % 2 == 0:
                    new_idx = idx == 0 and 1 or idx - 2
                elif idx % 2 == 1:
                    new_idx = idx == len(l) - 2 and idx + 1 or idx + 2
                _teams[new_idx] = n

        # sorting/ordering

        for i in range(len(_games)):

            indexes = []
            _tmp = list(_flatten(copy.copy(games)))
            _tmp.reverse()

            for idx, _game in enumerate(_games):
                # calc index
                index = -1
                for _team in _game:
                    if _team in _tmp:
                        _team_idx = _tmp.index(_team)
                        if _team_idx % 2 == 1:
                            _team_idx -= 1
                        index -= _team_idx
                    else:
                        index -= len(obj_list) * 2

                indexes.append(dict(game=idx, index=index))

            indexes.sort(key=lambda itm: itm.get('index'))

            game_idx = indexes[0].get('game')
            games.append(_games.pop(game_idx))

    # assign schedule to teams
    result = []
    for game in games:
        result.append((object_list[game[0]], object_list[game[1]]))

    return result


def scheduler_2vs2(object_list):

    obj_list = [idx for idx, obj in enumerate(object_list)]

    games, _games, _teams = [], [], []

    [_teams.append(t) for t in itertools.combinations(obj_list, 2)]

    for x in itertools.permutations(_teams, 2):
        if len(set(list(_flatten(x)))) == 4 and not (x[1], x[0]) in _games:
            _games.append(x)

    # sorting/ordering
    for i in range(len(_games)):

        _todo = copy.copy(_games)
        _done = copy.copy(games)
        _tmp = list(_flatten(_done))

        _teams_count = dict()
        for _team in _teams:
            _teams_count[_team] = len([1 for _game in _done if _team in _game])

        def _get_sort_index(item):

            idx = 0

            _item1, _item2 = item

            # count objects
            idx += _tmp.count(_item1[0]) * 2 + _tmp.count(_item1[1]) * 2
            idx += _tmp.count(_item2[0]) * 2 + _tmp.count(_item2[1]) * 2

            # teams count
            idx += _teams_count[_item1] + _teams_count[_item2]

            return idx

        def _sort(a, b):
            return _get_sort_index(a) - _get_sort_index(b)

        _todo.sort(key=functools.cmp_to_key(_sort))

        _game = _games.pop(_games.index(_todo[0]))
        games.append(_game)

    # reassign indexes to object_list
    result = []
    # count = len(games)
    for idx, game in enumerate(games):
        _game = (
            (object_list[game[0][0]], object_list[game[0][1]]),
            (object_list[game[1][0]], object_list[game[1][1]])
        )

        # FIXME: optimize this (if possible)
        if idx > 0 and (idx % 2 == 0 or idx % len(object_list) - 1 == 1):
            _game = (_game[1], _game[0])

        result.append(_game)

    # _stats = dict()
    # for _obj in object_list:
    #     _stats[_obj] = dict(home=0, away=0)
    # for _g in result:
    #     for _t in _g[0]:
    #         _t_stat = _stats[_t]
    #         _t_stat['home'] += 1
    #         _stats[_t] = _t_stat
    #     for _t in _g[1]:
    #         _t_stat = _stats[_t]
    #         _t_stat['away'] += 1
    #         _stats[_t] = _t_stat

    # print(_stats)

    return result


if __name__ == "__main__":

    numbers = ['#{0}'.format(x + 1) for x in range(5)]

    schedules = []

    # schedules.append(scheduler_1vs1(numbers))
    schedules.append(scheduler_2vs2(numbers))

    # output

    for games in schedules:

        print('\n{0} games generated\n'.format(len(games)))

        for idx, _g in enumerate(games):

            # if idx > 0 and idx % int(len(numbers) / 2) == 0:
            #     print('')

            home = isinstance(_g[0], tuple) and _g[0] or [_g[0]]
            away = isinstance(_g[1], tuple) and _g[1] or [_g[1]]

            print("#{0}: {1} vs. {2}".format(
                '%d' % (idx + 1,), ','.join(home), ','.join(away)))
