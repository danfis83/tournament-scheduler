# README #

Scripts for Python and JavaScript to generate a game schedule (matchplan, fixtures) for X players (either 1vs1 or 2vs2)

Known issues:

* 2vs2 ordering is not perfect
* 2vs2 home/away ordering is not balanced
* missing tests