"use strict";

class Scheduler {

  range (num) {
    return Array.apply(null, Array(num)).map(function (_, i) {return i;});
  }

  clone (obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  count (arr, item) {
    let json_item = JSON.stringify(item);
    return arr.filter(obj => {
      return JSON.stringify(obj) === json_item;
    }).length;
  }

  // generators

  generate_1vs1 (object_list) {

    let games = [];  // result

    const games_per_round = parseInt(object_list.length / 2, 10);
    let _teams = this.clone(object_list);

    if (object_list.length % 2 === 0) {
      // even
      //
      // 1 2  ->  1 3  ->  1 4
      // 3 4      4 2      2 3
      //
      // 1 2  ->  1 3  ->  1 5  ->  1 6  ->  1 4
      // 3 4      5 2      6 3      4 5      2 6
      // 5 6      6 4      4 2      2 3      3 5
      //

      const game_rounds = object_list.length - 1;

      this.range(game_rounds).forEach(r => {
        let _tmp = this.clone(_teams);

        this.range(games_per_round).forEach(i => {
          let game;

          if (r % 2 === 1 && i === 0) {
            game = [_tmp[1], _tmp[0]];
          } else {
            game = [_tmp[0], _tmp[1]];
          }

          // add game to result list
          games.push(game);

          // remove teams
          _tmp = _tmp.slice(2);
        });

        // spin the teams (clockwise)
        this.clone(_teams).forEach((team, idx) => {
          let new_idx;

          if (idx === 0)
            new_idx = idx;
          else if (idx === 1)
            new_idx = 3;
          else if (idx === 2)
            new_idx = 1;
          else if (idx % 2 === 0)
            new_idx = idx - 2;
          else if (idx % 2 === 1)
            new_idx = (idx === _teams.length - 1) ? idx - 1 : idx + 2;

          _teams[new_idx] = team;
        });

      });


    } else {
      // odd
      //
      // 1 2  ->  3 1  ->  5 3  ->  4 5  ->  2 4
      // 3 4      5 2      4 1      2 3      1 5
      // 5        4        2        1        3
      //

      const game_rounds = object_list.length;
      let _games = [];

      this.range(game_rounds).forEach(r => {
        let _tmp = this.clone(_teams);

        this.range(games_per_round).forEach(i => {
          let game = [_tmp[0], _tmp[1]];

          // add game to result list
          _games.push(game);

          // remove teams
          _tmp = _tmp.slice(2);
        });

        // spin the teams (clockwise)
        this.clone(_teams).forEach((team, idx) => {
          let new_idx;

          if (idx % 2 === 0)
            new_idx = (idx === 0) ? 1 : idx -2;
          else
            new_idx = (idx === _teams.length - 2) ? idx + 1 : idx + 2;

          _teams[new_idx] = team;
        });

      });

      // sorting/ordering
      this.range(_games.length).forEach(i => {

        let _indexes = [];
        let _tmp = [];
        games.forEach(game => {
          _tmp.push(game[0]);
          _tmp.push(game[1]);
        });
        _tmp.reverse();

        _games.forEach((_game, idx) => {
          // calc game index
          let _index = -1;
          _game.forEach(_team => {
            let _team_index = _tmp.indexOf(_team);
            if (_team_index > -1) {
              if (_team_index % 2 === 1)
                _team_index--;
              _index -= _team_index;
            } else
              _index -= _teams.length * 2;
          });

          _indexes.push({game_idx: idx, index: _index});

        });

        _indexes.sort((a, b) => {return a.index - b.index});

        games.push(_games[_indexes[0].game_idx]);
        _games.splice(_indexes[0].game_idx, 1);
      });

    }

    return games;
  }

  generate_2vs2 (object_list) {

    let games = [];  // result

    // create teams/pairs
    let _teams = [];

    object_list.forEach(_obj1 => {
      object_list.forEach(_obj2 => {
        let _team = [_obj1, _obj2].sort();
        if (_obj1 != _obj2 && this.count(_teams, _team) === 0)
          _teams.push(_team);
      });
    });

    // generate (unordered) games
    let _games = [];

    _teams.forEach(_team1 => {
      _teams.forEach(_team2 => {
        if (_team1 !== _team2 && this.count(_team2, _team1[0]) === 0 &&
            this.count(_team2, _team1[1]) === 0) {

          let _pair = [_team2, _team1];

          if (this.count(_games, _pair) === 0 &&
              this.count(_games, _pair.reverse()) === 0)
            _games.push(_pair);
        }
      });
    });

    // TODO: sorting/ordering

    let _teams_count = {};

    this.range(_games.length).forEach(i => {
      let _done = this.clone(games);
      let _todo = this.clone(_games);
      let _tmp = [];
      _done.forEach(_team => {
        _tmp.push(_team[0][0]);
        _tmp.push(_team[0][1]);
        _tmp.push(_team[1][0]);
        _tmp.push(_team[1][1]);
      });

      // count teams
      _teams.forEach(_team => {
        let _json_team = JSON.stringify(_team);
        let _count = _done.filter(_obj => {
          return (JSON.stringify(_obj[0]) == _json_team ||
                  JSON.stringify(_obj[1]) == _json_team);
        });
        _teams_count[_team] = _count.length;
      });

      // sorted index helper
      const _get_sort_index = (_item => {

        const count_factor = 2;

        let _idx = 0;

        let _item1 = _item[0];
        let _item2 = _item[1];

        // count objects
        _idx += this.count(_tmp, _item1[0]) * count_factor;
        _idx += this.count(_tmp, _item1[1]) * count_factor;
        _idx += this.count(_tmp, _item2[0]) * count_factor;
        _idx += this.count(_tmp, _item2[1]) * count_factor;

        // teams count
        _idx += _teams_count[_item1];
        _idx += _teams_count[_item2];

        return _idx;

      });

      // sort by "index"
      _todo.sort((a, b) => {
        let _idx = _get_sort_index(a) - _get_sort_index(b);

        // NOTE: this is weird, but apparently the comparison for 0 is not
        // string based per default (which will result in "illogical" results)
        if (_idx === 0 && JSON.stringify(a) < JSON.stringify(b))
          _idx = -1;
        else if (_idx === 0 && JSON.stringify(a) > JSON.stringify(b))
          _idx = 1;

        return _idx;
      });

      let game_idx = -1;
      _games.forEach((_game, idx) => {
        if (JSON.stringify(_game) == JSON.stringify(_todo[0])) {
          game_idx = idx;
          return;
        }
      });

      if (game_idx > -1) {
        games.push(_games[game_idx]);
        _games.splice(game_idx, 1);
      } else
        console.warn('\nGame could not be matched from TODO list!\n');

    });

    // sort/reorder games
    games.forEach((game, idx) => {
      // FIXME: optimize this (if possible)
      if (idx > 0 && (idx % 2 === 0 || idx % object_list.length - 1 === 1))
        game.reverse();
    });

    return games;
  }

}

const numbers = ['#1', '#2', '#3', '#4', '#5'];
let scheduler = new Scheduler();

// let games = scheduler.generate_1vs1(numbers);
let games = scheduler.generate_2vs2(numbers);

console.log(`\n${games.length} games generated\n`);

games.forEach((item, idx) => {
  console.log(`#${idx + 1}: ${item[0]} vs. ${item[1]}`);
});
